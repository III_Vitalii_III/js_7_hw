// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, 
// якщо рядок є паліндромом (читається однаково зліва направо і справа наліво), або false в іншому випадку


function isPalindrome(str) {
   const length = str.length;
   for (let i = 0; i < length / 2; i++) {
       if (str[i] !== str[length - 1 - i]) {
           return false;
       }
   }
   return true;
}

console.log(isPalindrome('34543')); 
console.log(isPalindrome('34567')); 



// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити,
// максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false,
// якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
// funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false

function checkStringLength() {
   const str = prompt("Введіть рядок:");
   const maxLength = prompt("Введіть максимальну довжину:");

   const maxLen = parseInt(maxLength, 10);

   if (str.length <= maxLen) {
       return true;
   } else {
       return false;
   }
}

const result = checkStringLength();
console.log(result);


// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
// Функція повина повертати значення повних років на дату виклику функцію.

function calculateAge() {
   let birthDate = prompt("Введіть вашу дату народження (у форматі РРРР-ММ-ДД):");

   let birthDateObj = new Date(birthDate);

   if (isNaN(birthDateObj.getTime())) {
       return "Некоректна дата народження.";
   }

   let today = new Date();

   let birthYear = birthDateObj.getFullYear();
   let birthMonth = birthDateObj.getMonth();
   let birthDay = birthDateObj.getDate();

   let currentYear = today.getFullYear();
   let currentMonth = today.getMonth();
   let currentDay = today.getDate();

   let age = currentYear - birthYear;

   if (currentMonth < birthMonth || (currentMonth === birthMonth && currentDay < birthDay)) {
       age--;
   }

   return age;
}

console.log(calculateAge());
